import express, { json } from "express";
import { port } from "./constant.js";
import connectToMongoDB from "./src/connectToDb/connectToMongoDb.js";
import userRouter from "./src/router/userRouter.js";
import bookingRouter from "./src/router/bookingRouter.js";
import parkingRouter from "./src/router/parkingRouter.js";
let expressApp = express();
expressApp.use(json());
expressApp.listen(port, () => {
  console.log(`Express application is listening to ${port}`);
});
connectToMongoDB();
expressApp.use("/users", userRouter);
expressApp.use("/bookings", bookingRouter);
expressApp.use("/parkings", parkingRouter);
