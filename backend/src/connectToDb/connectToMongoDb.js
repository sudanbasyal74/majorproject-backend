import mongoose from "mongoose";
import { database, databaseLink } from "../../constant.js";
let connectToMongoDB = async () => {
  try {
    await mongoose.connect(databaseLink);
    // await mongoose.connect(database);
    console.log(`application is connected to mongodB database successfully`);
  } catch (error) {
    console.log(error.message);
  }
};

export default connectToMongoDB;
