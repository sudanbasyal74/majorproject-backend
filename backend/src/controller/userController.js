import { Booking, User } from "../schema/model.js";
import bcrypt from "bcrypt";
import { sendEmail } from "../utils/sendmail.js";
import jwt from "jsonwebtoken";
import { secretKey } from "../../constant.js";
export let createUser = async (req, res) => {
  let userData = req.body;
  let password = userData.password;
  let email = userData.email;
  let firstName = userData.firstName;
  try {
    let hashedpassword = await bcrypt.hash(password, 10);
    userData.password = hashedpassword;
    let users = await User.create(userData);
    await sendEmail({
      from: "'Smart-Parking-System'<noreplysmartparkingsystem@gmail.com>",
      to: [email],
      subject: "Account Created Successfully",
      html: `<h1>Welcome to smart parking system ${firstName}</h1>`,
    });
    res.status(201).json({
      success: true,
      message: "user created successfully",
      result: users,
    });
  } catch (error) {
    res.status(409).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUser = async (req, res) => {
  try {
    let users = await User.find({}).populate("booking");
    res.status(200).json({
      success: true,
      message: "Data read successfully",
      users: users,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUserDetails = async (req, res) => {
  let userID = req.params.userId;
  try {
    let result = await User.findById(userID).populate("booking");
    // console.log(result.booking[0]._id);
    res.status(200).json({
      success: true,
      message: "userData read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUserBookingDetails = async (req, res) => {
  let userID = req.params.userId;
  try {
    let result = await Booking.findOne({ userId: userID });
    res.status(200).json({
      success: true,
      message: "Booking Data for the user read successfully",
      booking: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateUser = async (req, res) => {
  let userID = req.params.userId;
  let userData = req.body;
  try {
    let result = await User.findByIdAndUpdate(userID, userData);
    res.status(200).json({
      success: true,
      message: "Data updated successfully",
      users: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUser = async (req, res) => {
  let userID = req.params.userId;
  console.log(userID);
  try {
    let result = await User.findByIdAndDelete(userID);
    res.status(200).json({
      success: true,
      message: "userData is deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUser = async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  try {
    let user = await User.findOne({ email: email }); //searches any user with entered email
    let hashPassword = user.password; //hashed password stored in the database
    if (user === null) {
      res.status(409).json({
        success: false,
        message: "Email or password does not match ",
      });
    } else {
      let isValidUser = await bcrypt.compare(password, hashPassword);
      if (isValidUser) {
        //generate token

        let infoObj = {
          id: user._id,
        };

        let expiryInfo = {
          expiresIn: "365d",
        };
        let token = jwt.sign(infoObj, secretKey, expiryInfo);

        res.status(201).json({
          success: true,
          message: "user logged in successfully",
          result: token,
        });
      } else {
        res.status(400).json({
          success: false,
          message: "email or password does not match",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
