import { Parking } from "../schema/model.js";
export let viewParking = async (req, res) => {
  try {
    let information = await Parking.find({});
    res.json({
      success: true,
      message: "Parking information read successfully",
      information: information,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let resetParking = async (req, res) => {
  try {
    let ParkingSpace = 0;
    let BookedSpace = 0;
    let result = await Parking.create({
      AvailableSpace: ParkingSpace,
      BookedSpace: BookedSpace,
    });
  } catch (error) {}
};

// to add number of bookings
// let length = information.length;
// let ParkingSpace = information[length - 1].AvailableSpace;
// let BookedSpace = length + 1;
// let result = await Parking.create({
//   AvailableSpace: ParkingSpace,
//   BookedSpace: BookedSpace,
// });
// console.log(result);
