import mongoose, { startSession } from "mongoose";
import { Booking, Parking, User } from "../schema/model.js";
import { sendEmail } from "../utils/sendmail.js";

export let createBooking = async (req, res) => {
  let bookingData = req.body;
  let userID = bookingData.userId;
  try {
    let validUser = await User.findById(userID);

    let email = validUser.email;
    let information = await Parking.find({});
    let length = information.length;
    let ParkingSpace = information[length - 1].AvailableSpace;
    let BookedSpace;
    // console.log(email);
    // console.log(validUser._id);
    let _booking = await Booking.findOne({ userId: validUser._id });
    if (_booking) {
      res.json({
        success: false,
        message: "booking exists",
      });
    } else {
      if (validUser) {
        let booking = await Booking.create(bookingData);
        let numberOfBooking = await Booking.find({});
        BookedSpace = numberOfBooking.length;
        let result = await Parking.create({
          AvailableSpace: ParkingSpace,
          BookedSpace: BookedSpace,
        });
        validUser.booking = booking._id;
        let session = await startSession();
        session.startTransaction();
        await validUser.save();
        session.commitTransaction();
        await sendEmail({
          from: "'Smart-Parking-System'<noreplysmartparkingsystem@gmail.com>",
          to: [email],
          subject: "Parking Booked Successfully",
          html: `<h1>Parking booked successfully for ${validUser.firstName} ${validUser.lastName} with vehicle number ${booking.licenseNumber}</h1>`,
        });
        res.json({
          success: true,
          message: "User Found",
          result: booking,
        });
      } else {
        res.json({
          success: false,
          message: "User Not Found",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export default createBooking;

export let readBooking = async (req, res) => {
  try {
    let booking = await Booking.find({}).populate("userId");
    res.status(200).json({
      success: true,
      message: "Booking Data read successfully",
      booking: booking,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readBookingDetails = async (req, res) => {
  let bookingID = req.params.bookingId;
  try {
    let result = await Booking.findById(bookingID).populate("userId");
    // console.log(result);
    res.status(200).json({
      success: true,
      message: "userData read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateBooking = async (req, res) => {
  let bookingID = req.params.bookingId;
  let bookingData = req.body;
  try {
    let result = await Booking.findByIdAndUpdate(bookingID, bookingData);
    res.status(200).json({
      success: true,
      message: "Data updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteBooking = async (req, res) => {
  let bookingID = req.params.bookingId;
  try {
    let result = await Booking.findByIdAndDelete(bookingID).populate("userId");
    let information = await Parking.find({});
    let length = information.length;
    let ParkingSpace = information[length - 1].AvailableSpace;
    let numberOfBooking = await Booking.find({});
    let BookedSpace = numberOfBooking.length;
    let _result = await Parking.create({
      AvailableSpace: ParkingSpace,
      BookedSpace: BookedSpace,
    });
    res.status(200).json({
      success: true,
      message: "Booking is deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
