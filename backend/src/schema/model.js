import { model } from "mongoose";
import userSchema from "./userSchema.js";
import bookingSchema from "./bookingSchema.js";
import parkingSchema from "./parkingSchema.js";

export let User = model("User", userSchema);
export let Booking = model("Booking", bookingSchema);
export let Parking = model("Parking", parkingSchema);
