import mongoose, { Schema } from "mongoose";

let userSchema = Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      minLength: 6,
    },
    booking: [
      {
        type: Schema.ObjectId,
        ref: "Booking",
        required: true,
        default: "",
      },
    ],
  },
  {
    timestamps: true,
  }
);

export default userSchema;
