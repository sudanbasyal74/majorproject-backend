import { Schema } from "mongoose";

let parkingSchema = Schema({
  AvailableSpace: {
    type: Number,
    required: true,
    default: 1,
  },
  BookedSpace: {
    type: Number,
    required: true,
    default: 1,
  },
});
export default parkingSchema;
