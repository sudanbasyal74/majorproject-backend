import mongoose, { Schema } from "mongoose";

let bookingSchema = Schema(
  {
    userId:
      {
        type: Schema.ObjectId,
        ref: "User",
        required: true,
      },
    licenseNumber: {
      type: String,
      required: true,
    },
    date: {
      type: Date,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

export default bookingSchema;
