import { Router } from "express";
import createBooking, {
    deleteBooking,
  readBooking,
  readBookingDetails,
  updateBooking,
} from "../controller/bookingController.js";

let bookingRouter = Router();
bookingRouter.route("/").post(createBooking).get(readBooking);
bookingRouter
  .route("/:bookingId")
  .get(readBookingDetails)
  .patch(updateBooking)
  .delete(deleteBooking);
export default bookingRouter;
