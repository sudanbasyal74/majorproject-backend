import { Router } from "express";
import {
  createUser,
  deleteUser,
  loginUser,
  readUser,
  readUserBookingDetails,
  readUserDetails,
  updateUser,
} from "../controller/userController.js";

let userRouter = Router();
userRouter.route("/").get(readUser);
userRouter.route("/signup").post(createUser);
userRouter.route("/login").post(loginUser);
userRouter.route("/booking/:userId").get(readUserBookingDetails);
userRouter
  .route("/:userId")
  .get(readUserDetails)
  .patch(updateUser)
  .delete(deleteUser);
export default userRouter;
// 1:08:13 / 7:22:36
