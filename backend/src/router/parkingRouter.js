import { Router } from "express";
import { resetParking, viewParking } from "../controller/parkingController.js";
let parkingRouter = Router();
parkingRouter.route("/").get(viewParking).post(resetParking);
export default parkingRouter;
