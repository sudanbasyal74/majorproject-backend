import { config } from "dotenv";
config();

export let port = process.env.PORT;
export let databaseLink = process.env.DATABASE_LINK;
export let database = process.env.DATABASE;
export let email = process.env.EMAIL;
export let password = process.env.PASSWORD;
export let secretKey = process.env.SECRET_KEY;
